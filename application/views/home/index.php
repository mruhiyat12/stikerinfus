 <!-- Content Wrapper -->
 <div id="content-wrapper" class="d-flex flex-column">

     <!-- Main Content -->
     <div id="content">

         <!-- Topbar -->
         <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


             <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100">
                 <img src="<?= base_url('assets/img/gif rspk.gif'); ?>" height="60" alt="">
             </div>


         </nav>
         <!-- End of Topbar -->

         <!-- Begin Page Content -->
         <div class="container-fluid">

             <!-- Page Heading -->
             <div class="d-sm-flex align-items-center justify-content-between mb-4">
                 <h1 class="h3 mb-0 text-gray-800">PRINT STIKER INFUS</h1>
             </div>



             <!-- Content Row -->

             <div class="row">

                 <!-- Area Chart -->
                 <div class="col-xl col-lg">
                     <div class="card shadow mb-4">
                         <!-- Card Header - Dropdown -->
                         <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">


                         </div>
                         <!-- Card Body -->
                         <div class="card-body ">
                             <div class=" ">
                                 <!-- <canvas id="myAreaChart"></canvas> -->
                                 <form action="<?= base_url('cetak'); ?>" method="POST">
                                     <div class="row">
                                         <div class="form-group mb-3 col-lg-5 ">

                                             <label for="nama" class="col-sm col-form-label">Nama Pasien</label>
                                             <div class="input-group col-sm">
                                                 <input type="text" class="form-control" id="nama" name="nama">
                                                 <select class="form-select form-control col-lg-3" id="selection" name="selection">
                                                     <option selected>Pilih..</option>
                                                     <option value="Tn">Tn</option>
                                                     <option value="Nn">Nn</option>
                                                     <option value="Ny">Ny</option>
                                                     <option value="An">An</option>
                                                     <option value="By">By</option>
                                                 </select>
                                             </div>

                                             <label for="cairaninfus" class="col-sm col-form-label">Cairan Infus</label>
                                             <div class="col-sm">
                                                 <input type="text" class="form-control" id="cairaninfus" name="cairaninfus">
                                             </div>

                                             <label for="kolf" class="col-sm col-form-label">Kolf Ke</label>
                                             <div class="col-sm">
                                                 <input type="text" class="form-control" id="kolf" name="kolf">
                                             </div>

                                             <label for="tetesan" class="col-sm col-form-label">JML Tetesan</label>
                                             <div class="col-sm">
                                                 <input type="text" class="form-control" id="tetesan" name="tetesan">
                                             </div>

                                             <label for="obattambahan" class="col-sm col-form-label">Obat yang di Tambah</label>
                                             <div class="col-sm">
                                                 <textarea class="form-control" id="obattambahan" name="obattambahan"></textarea>
                                             </div>

                                         </div>




                                         <div class="form-group mb-3 col-lg-5">

                                             <label for="jumlah" class="col-sm col-form-label">Jumlah</label>
                                             <div class="col-sm">
                                                 <textarea type="text" class="form-control" id="jumlah" name="jumlah"></textarea>
                                             </div>

                                             <label for="tgl" class="col-sm col-form-label">Tanggal Pemberian</label>
                                             <div class="col-sm">
                                                 <input type="date" class="form-control" id="tgl" name="tgl">
                                             </div>

                                             <label for="jpemberi" class="col-sm col-form-label">Diberikan Oleh</label>
                                             <div class="col-sm">
                                                 <input type="text" class="form-control" id="pemberi" name="pemberi">
                                             </div>

                                             <label for="jam" class="col-sm col-form-label">Jam</label>
                                             <div class="col-sm">
                                                 <input type="time" class="form-control" id="jam" name="jam">
                                             </div>

                                             <label for="dicek" class="col-sm col-form-label">Dicek Oleh</label>
                                             <div class="col-sm">
                                                 <input type="text" class="form-control" id="dicek" name="dicek">
                                             </div>

                                         </div>
                                     </div>
                                     <button type="submit" class="btn btn-success"><i class="fas fa-print"></i> PRINT</button>

                                 </form>

                             </div>
                         </div>
                     </div>
                 </div>

                 <!-- Pie Chart -->

             </div>



         </div>
         <!-- /.container-fluid -->

     </div>
     <!-- End of Main Content -->